<?php 

/**
 * array_map => Map: Applica a tutti gli elementi una
 * stessa sequenza di codice (è l'operazione più generale) 
 * restituendo un nuovo array in cui gli elementi sono 
 * il risultato dell'applicazione della funzione al rispettivo 
 * valore dell'array
 */

function cube($n)
{
	return ($n * $n * $n);
}

$a = [1, 2, 3, 4, 5];
$b = array_map('cube', $a);

print_r($b);


/*
// In maniera più "criptica"
$b = array_map( function($n) { 
		return ($n * $n * $n);
	}, $a);

*/


/*
// Esempio sul sito: https://www.php.net/manual/en/functions.anonymous.php

preg_replace_callback('~-([a-z])~', function ($match) {
    return strtoupper($match[1]);
}, 'hello-world'); 
*/
