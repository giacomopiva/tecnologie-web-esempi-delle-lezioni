<?php 

class Person 
{
	// proprietà

	public $first_name = "John";
	public $last_name = "Doe";
	
	// metodi
	
	public function getFullName() 
	{
		return $this->first_name . " " . $this->last_name . "\n";
	}
}

$p = new Person();

echo $p->getFullName();

?>
