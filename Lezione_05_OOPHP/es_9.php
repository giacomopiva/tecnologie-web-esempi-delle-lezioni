<?php 

/**
 * Array 
 */

$values = array('value1', 'value2', 'value3', 'value4');

print_r($values);

for ($i=0; $i < count($values); $i++) {
	echo "$i -> $values[$i] \n";
}

/**
 * Array associativi
 */

$values = array(
	'Key1' => 'value1', 
	'Key2' => 'value2', 
	'Key3' => 'value3', 
	'Key4' => 'value4');

print_r($values);

// Un array associativo non ha indici, quindi non posso usare [$i] 
foreach ($values as $value) {
	echo "$value \n";
}

// Se ho bisogno dell'indice posso usare: 
foreach ($values as $key => $value) {
	echo "$key -> $value \n";
}

// Non è possibile usare echo $values[1] cse $values è un array associativo
?>
