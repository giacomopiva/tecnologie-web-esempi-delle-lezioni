<?php 

class Point 
{	
	private $x;
	private $y;

	public function __construct() 
	{
		$this->x = 0; 
		$this->y = 0;
	}
		
	public function setPosition($a, $b) 
	{
		$this->x = $a; 
		$this->y = $b; 		
	}

	public function getPosition() 
	{
		return "X: $this->x, Y: $this->y"; 
	}
}

$p = new Point();

echo $p->getPosition() . "\n";

$p->setPosition(5, 8);

echo $p->getPosition() . "\n";

?>
