<?php 

/**
 * array_filter => Filter: Applica a tutti gli elementi un  
 * controllo logico restituendo un array con i soli elementi per 
 * cui il controllo ha restituito "vero".
 */

function even($var)
{
	return ($var % 2 == 0);
}

function odd($var)
{
	return !even($var);
}

$array1 = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];
$array2 = [6, 7, 8, 9, 10, 11, 12];

echo "Dispari:\n";
print_r(array_filter($array1, "odd")); // Tutti gli elementi dispari

echo "Pari:\n";
print_r(array_filter($array2, "even")); // tutti gli elementi pari
