<?php 

/**
 * Ad una proprietà o un metodo marcato come protected possono 
 * accedere tutti i metodi che sono definiti all’interno della 
 * classe o all’interno di sotto classi.
 */

class Point 
{
	protected $x;
	protected $y;
	
	public function __construct() 
	{
		$this->x = 0; 
		$this->y = 0;
	}
		
	public function setPosition($a, $b) 
	{
		$this->x = $a; 
		$this->y = $b; 		
	}

	public function getPosition() 
	{
		return "X: $this->x, Y: $this->y"; 
	}
}

/** 
 * La classe Pixel estende la classe Point 
 * La classe Pixel è sottoclasse della classe Point
 * La classe Point è superclasse della classe Pixel
 */

class Pixel extends Point 
{
	private $color;
	
	public function __construct() 
	{
		parent::__construct();
		$this->color = "#000000";
	}
		
	public function setColor($c) 
	{
		$this->color = $c;  		
	}

	public function getPosition() 
	{
		return "X: $this->x, Y: $this->y of color: $this->color"; 
	}
}

$p = new Pixel();
echo $p->getPosition() . "\n";

$p->setPosition(5, 8);

$p->setColor("#FF0000");
echo $p->getPosition() . "\n";

?>
