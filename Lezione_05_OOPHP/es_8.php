<?php 

/**
 * Ad una proprietà o un metodo marcato come protected possono 
 * accedere tutti i metodi che sono definiti all’interno della 
 * classe o all’interno di sotto classi.
 */

class Point {
	protected $x;
	protected $y;
	
	public function __construct() 
	{
		$this->x = 0; 
		$this->y = 0;
	}
		
	public function setPosition($x, $y) 
	{
		$this->x = $x; 
		$this->y = $y; 		
	}

	public function getPosition() 
	{
		return "X: $this->x, Y: $this->y"; 
	}
}

/** 
 * La classe Pixel estende la classe Point 
 * La classe Pixel è sottoclasse della classe Point
 * La classe Point è superclasse della classe Pixel
 */

class Pixel extends Point {
	private $color;
	
	public function __construct() 
	{
		parent::__construct();
		$this->color = "#000000";
	}
		
	public function setColor($c) 
	{
		$this->color = $c;  		
	}

	public function getPosition() 
	{
		return "X: $this->x, Y: $this->y of color: $this->color"; 
	}
}

$a = new Pixel();
echo $a->getPosition() . "\n";

$a->setPosition(5, 8);	// Un Pixel È un Point quindi posso usare i metodi della classe padre
echo $a->getPosition() . "\n";

?>

