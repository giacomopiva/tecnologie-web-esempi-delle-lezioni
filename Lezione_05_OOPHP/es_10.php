<?php 

/**
 * Array multidimensionali
 */

$rows = array();

/* 1° Elemento */

$data = array(
	'Nome' => 'Mario', 
	'Cognome' => 'Rossi', 
	'Salario' => '20.000');
	
array_push($rows, $data);

/* 2° Elemento */

$data = array(
	'Nome' => 'Alberto', 
	'Cognome' => 'Bianchi', 
	'Salario' => '21.000');
	
array_push($rows, $data);

/* 3° Elemento */

$data = array(
	'Nome' => 'Giulio', 
	'Cognome' => 'Neri', 
	'Salario' => '26.000');
	
array_push($rows, $data);

// Al termine visualizzo tutto l'Array ... 
print_r($rows);

// Visualizzo il Cognome del secondo elemento dell'array $rows
echo $rows[1]['Cognome'] . "\n";

// $rows è l'insieme delle righe
// $rows[1] è il secondo elemento
// $rows[1]['Cognome'] è il valore di 'Cognome' del secondo elemento

?>
