<?php 

class Person 
{
	// proprietà
	private $first_name;
	private $last_name;
	
	// costruttore
	public function __construct($fn, $ln) 
	{
		$this->first_name = $fn; 
		$this->last_name = $ln;
	}
	
	// metodi
	public function getFullName() 
	{
		return $this->first_name . " " . $this->last_name;
	}
}

$john = new Person("John", "Doe");
$tom = new Person("Tom", "Ford");

echo $john->getFullName() . "\n";
echo $tom->getFullName() . "\n";

?>
