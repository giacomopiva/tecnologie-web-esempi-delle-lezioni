<?php 

class Person 
{
	// proprietà
	
	private $first_name;
	private $last_name;
	
	// costruttore
	public function __construct($fn, $ln) 
	{
		$this->first_name = $fn; 
		$this->last_name = $ln;
	}
	
	// metodi
	
	// metodi getter e setter
	
	public function setFirstName($fn) 
	{
		$this->first_name = $fn; 
	}

	public function setLastName($ln) 
	{
		if (strlen($ln) > 0) {
			$this->last_name = $ln; 
		} else {
			$this->last_name = "Missing Last name"; 
		}
	}

	public function getFirstName() 
	{
		return $this->first_name; 
	}

	public function getLastName() 
	{
		return $this->last_name; 
	}

	// altri metodi
	public function getFullName() 
	{
		return $this->first_name . " " . $this->last_name;
	}
}

$john = new Person("John", "Doe");
echo $john->getFullName() . "\n";

$john->setLastName("Travolta");
echo $john->getFullName() . "\n";


//$john->last_name = "Wayne"; 		// Non lo posso fare!
//echo $john->getFullName() . "\n";	// Non verrà mai eseguita

?>
