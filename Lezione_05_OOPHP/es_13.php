<?php 

/**
 * array_reduce => Reduce: Applica a tutti gli elementi una  
 * stessa sequenza di codice con lo scopo di "aggregare" gli
 * elementi dell'array fino ad ottenere un valore unico 
 * esempio la somma degli elementi dell'array stesso
 *
 * $carry, è l'elemento sul quale si "riduce" ovvero il valore 
 * di ciò che è successo fino ad ora con i precedenti valori.
 */

function sum($carry, $item)
{
    $carry += $item;
    return $carry;
}

function product($carry, $item)
{
    $carry *= $item;
    return $carry;
}

$a = array(1, 2, 3, 4, 5);

var_dump(array_reduce($a, "sum")); 
var_dump(array_reduce($a, "product", 10)); // 10 è il valore iniziale (opzionale)
