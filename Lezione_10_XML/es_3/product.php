<?php 
	// Preparo un array per contenere i valori
	$data = array();
	
	// Con un ciclo for popolo l'array con alcuni dati 
	for ($i = 0; $i < 5; $i++) {
		array_push( $data, array(	'name' => 'Nike Air', 
									'image' => 'http://placehold.it/600x600.jpg', 
									'price' => '60.00') );
	}

	// Visualizzo l'array ...
	//echo "<pre>";
	//print_r($data);
	//echo "</pre>";
	//die();
	
	// Eseguo l'encoding dell'array 			
	$json = json_encode($data);

	// Restituisco l'array serializzato come JSON visualizzandolo
	echo $json;
?>
